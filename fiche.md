# Ateliers Téléphones Libres - 10/12/2018

Cette fiche a pour but de partager les bonnes pratiques d'hygiène numérique et ce qui peut être fait pendant l'atelier.

Tout  cela peut-être fait sur un téléphone Google Android, si l'utilisateur ne souhaite pas remplacer tout le système d'exploitation.

## Général

- supprimer le compte Google du téléphone (paramètres, comptes, Google, options, supprimer compte)
- idem pour les autres comptes non utilisés
- Installation système alternatif :
    - Lineage for microG : https://lineage.microg.org (permet d'utiliser les applis nécessitant les services Google)

## Services nécessitant la création d'un compte

- Synchro contacts :
    - Framagenda
- Synchro agenda :
    - Framagenda
- Synchro notes :
    - Framagenda
- Mail :
    - ProtonMail
    - Riseup (sur invitation)
- VPN :
    - ProtonVPN
    - Bitmask (Riseup)
- RSS :
    - Framanews

## Conseils

- utiliser un gestionnaire de mot de passe (LessPass, Keepass, ...)
- réduire les notification des applications (hygiène numérique)

## Config Firefox

- desactiver cookies tiers + supprimer cookies à la fermeture de FF (options, paramètres, vie privée)
- changer moteur de recherche pour :
    - Qwant
- Installer modules :
    - uBlock ;
    - HTTPS everywhere

## Applications grand public

F : dispo sur f-droid | G : appli libre dispo sur PlayStore | G- : appli proprio dispo sur PlayStore

- F f-droid : magasin d'application alternatif (télécharger depuis le site officiel) ;
- F Yalp : pour télécharger les applications du PlayStore sans le compte Google : depuis f-droid ;
- F Firefox (ou Fennec sur f-droid) : navigateur web
- F K-9 mail : client mail
- G ProtonMail (utilise les Google Services mais fonctionne sans)
- G- App-sauvegarde : pour sauvegarder SMS, appels et liste de contacts
- F Slight backup : gestionnaire de sauvegarde (à tester)
- G CozyCloud : Cloud personnel
- F Notes nextcloud : prise de notes (à synchroniser avec compte framagenda)
- F Notes NextCloud : Synchro notes avec Nextcloud (type OneDrive, markdown only)
- F davdroid : synhro contacts/agenda (à synchroniser avec compte framagenda)
- F ICSdroid (synchro agendas externes ics)
- F OSMAnd : carte OpenStreetMap
- G Signal : messages et appels chiffrés, comme WhatsApp (passe par les données)
- G Silence : SMS chiffrés (ne protège pas les métadonnées)
- F VLC : player vidéo
- F PeerTube : application pour regarder des vidéos hébergées sur une instance PeerTube

## Application pour utilisateurs avancés

- F EasyRSS : client RSS (à synchroniser avec compte framanews)
- F Exodus Privacy : détecteur de mouchards
- F LessPass : gestionnaire de mot de passe sans stockage
- F KeepassDroid : gestion de mdp avec stockage et synchro (sur NextCloud par ex.)
- F AndOTP : authentification à 2 facteurs
- F OpenVPN for Android : client VPN
- F Mastalab : client Mastodon
- F Riot.im : client Matrix

## Ne concerne pas spécialement la vie privée, mais à tester

- F RadioParadise : Radio, avec cache de 6 heures (bien pratique)
- F Amaze : explorateur de fichiers
